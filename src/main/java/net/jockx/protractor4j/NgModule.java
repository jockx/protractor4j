package net.jockx.protractor4j;

/**
 * Represents an Angular module to load before Angular is loaded.
 * <p>
 * The modules will be registered after existing modules,
 * so any module registered will override preexisting modules with the same name.
 */
public class NgModule {

	private String name;

	private String script;

	/**
	 * Creates a new instance of {@link NgModule}
	 *
	 * @param name   The name of the module to load or override.
	 * @param script The JavaScript to load the module.
	 */
	public NgModule(String name, String script) {
		this.name = name;
		this.script = script;
	}

	/**
	 * Name of the module to load or override.
	 */
	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	/**
	 * JavaScript to load the module.
	 */
	public String getScript() {
		return script;
	}

	protected void setScript(String script) {
		this.script = script;
	}

}
