package net.jockx.protractor4j;

import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsDriver;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides a mechanism to write tests against an AngularJS application.
 */
public class NgWebDriver implements WebDriver, WrapsDriver {

	private final String angularDeferBootstrap = "NG_DEFER_BOOTSTRAP!";
	public boolean ignoreSynchronization;
	private WebDriver driver;
	private JavascriptExecutor jsExecutor;
	private String rootElement;
	private NgModule[] mockModules;

	/**
	 * Creates a new instance of {@link NgWebDriver} by wrapping a {@link WebDriver} instance.
	 *
	 * @param driver      The configured webdriver instance.
	 * @param mockModules The modules to load before Angular whenever Url setter or Navigate().to() is called.
	 */
	public NgWebDriver(WebDriver driver, NgModule[] mockModules) {
		this(driver, "body", mockModules);
	}

	/**
	 * Creates a new instance of {@link NgWebDriver} by wrapping a {@link WebDriver} instance.
	 *
	 * @param driver      The configured webdriver instance.
	 * @param rootElement The CSS selector for an element on which to find Angular.
	 *                    This is usually 'body' but if your ng-app is on a subsection of the page it may be a subelement.
	 * @param mockModules The modules to load before Angular whenever Url setter or Navigate().to() is called.
	 */
	public NgWebDriver(WebDriver driver, String rootElement, NgModule[] mockModules) {
		if (!(driver instanceof JavascriptExecutor)) {
			throw new UnsupportedOperationException("The WebDriver instance must implement the IJavaScriptExecutor interface.");
		}
		this.driver = driver;
		this.jsExecutor = (JavascriptExecutor) driver;
		this.rootElement = rootElement;
		this.mockModules = mockModules;
	}

	/**
	 * Sets the URL the browser is currently displaying.
	 *
	 * @param url The URL to load. It is best to use a fully qualified URL
	 */
	@Override
	public void get(String url) {
		// TODO: test Safari & Android
		HasCapabilities hcDriver = (HasCapabilities) this.driver;
		if (hcDriver != null &&
				(Objects.equals(hcDriver.getCapabilities().getBrowserName(), "internet explorer") ||
						Objects.equals(hcDriver.getCapabilities().getBrowserName(), "phantomjs"))) {
			// Internet Explorer & PhantomJS
			this.jsExecutor.executeScript("window.name += '" + angularDeferBootstrap + "';");
			this.driver.get(url);
		} else {
			// Chrome & Firefox
			this.driver.get("about:blank");
			this.jsExecutor.executeScript("window.name += '" + angularDeferBootstrap + "'; " +
					"window.location.href = '" + url + "';");
		}

		// Make sure the page is an Angular page.
		Object isAngularApp = this.jsExecutor.executeAsyncScript(ClientSideScripts.testForAngular, 10);
		if (isAngularApp instanceof Boolean && (Boolean) isAngularApp) {
			// At this point, Angular will pause for us, until angular.resumeBootstrap is called.

			// Register extra modules
			for (NgModule ngModule : this.mockModules) {
				this.jsExecutor.executeScript(ngModule.getScript());
			}
			// Resume Angular bootstrap
			this.jsExecutor.executeScript(ClientSideScripts.resumeAngularBootstrap,
					Arrays.asList(this.mockModules).stream()
							.map(NgModule::getName)
							.collect(Collectors.joining(",")));
		} else {
			throw new UnsupportedOperationException(
					"Angular could not be found on the page '" + url + "'");
		}
	}

	/**
	 * @return Gets the URL the browser is currently displaying.
	 */
	@Override
	public String getCurrentUrl() {
		this.waitForAngular();
		HasCapabilities hcDriver = (HasCapabilities) this.driver;
		if (hcDriver != null && Objects.equals(hcDriver.getCapabilities().getBrowserName(), "internet explorer")) {
			// 'this.driver.Url' does not work on IE
			return (String) this.jsExecutor.executeScript(ClientSideScripts.getLocationAbsUrl, this.rootElement);
		} else {
			return this.driver.getCurrentUrl();
		}
	}


	/**
	 * @return Gets the title of the current browser window.
	 */
	@Override
	public String getTitle() {
		this.waitForAngular();
		return this.driver.getTitle();
	}

	/**
	 * Finds all {@link NgWebElement}s within the current context
	 * using the given mechanism.
	 *
	 * @param by The locating mechanism to use.
	 * @return A list of all {@link NgWebElement}s
	 * matching the current criteria, or an empty list if nothing matches.
	 */
	@Override
	public List<WebElement> findElements(By by) {
		this.waitForAngular();
		return this.driver.findElements(by).stream()
				.map(e -> new NgWebElement(this, e))
				.collect(Collectors.toList());

	}

	/**
	 * Finds the first {@link NgWebElement} using the given mechanism.
	 *
	 * @param by The locating mechanism to use.
	 * @return The first matching {@link NgWebElement} on the current context.
	 * @throws NoSuchElementException if no element matches the criteria.
	 */
	@Override
	public NgWebElement findElement(By by) {
		this.waitForAngular();
		return new NgWebElement(this, this.driver.findElement(by));
	}

	/**
	 * @return Gets the source of the page last loaded by the browser.
	 */
	@Override
	public String getPageSource() {
		this.waitForAngular();
		return this.driver.getPageSource();
	}

	/**
	 * Close the current window, quitting the browser if it is the last window currently open.
	 */
	@Override
	public void close() {
		this.driver.close();
	}

	/**
	 * Quits this driver, closing every associated window.
	 */
	@Override
	public void quit() {
		this.driver.quit();
	}

	/**
	 * Gets the window handles of open browser windows.
	 *
	 * @return Window handles
	 */
	@Override
	public Set<String> getWindowHandles() {
		return this.driver.getWindowHandles();
	}

	/**
	 * Gets the window handle of open browser windows.
	 *
	 * @return Window handles
	 */
	@Override
	public String getWindowHandle() {
		return this.driver.getWindowHandle();
	}

	/**
	 * Instructs the driver to send future commands to a different frame or window.
	 *
	 * @return An {@link org.openqa.selenium.WebDriver.TargetLocator} object which can be used to select a frame or window.
	 */
	@Override
	public TargetLocator switchTo() {
		return null;
	}

	/**
	 * Instructs the driver to navigate the browser to another location.
	 *
	 * @return An {@link org.openqa.selenium.WebDriver.Navigation} object allowing the user to access
	 * the browser's history and to navigate to a given URL.
	 */
	@Override
	public Navigation navigate() {
		return new NgNavigation(this, this.driver.navigate());
	}

	/**
	 * Instructs the driver to change its settings.
	 *
	 * @return An {@link org.openqa.selenium.WebDriver.Options} object allowing the user to change the settings of the driver.
	 */
	@Override
	public Options manage() {
		return this.driver.manage();
	}

	/**
	 * Use this to interact with pages that do not contain Angular (such as a log-in screen).
	 *
	 * @return The wrapped {@link WebDriver} instance.
	 */
	@Override
	public WebDriver getWrappedDriver() {
		return this.driver;
	}

	/**
	 * @return The CSS selector for an element on which to find Angular. This is usually 'body' but if your ng-app is on a subsection of the page it may be a subelement.
	 */
	public String getRootElement() {
		return this.rootElement;
	}

	/**
	 * If true, Protractor will not attempt to synchronize with the page before performing actions.
	 * This can be harmful because Protractor will not wait until $timeouts and $http calls have been processed,
	 * which can cause tests to become flaky.
	 * This should be used only when necessary, such as when a page continuously polls an API using $timeout.
	 *
	 * @return true if synchronization is disabled, false otherwise
	 */
	public boolean isIgnoreSynchronization() {
		return ignoreSynchronization;
	}

	/**
	 * If true, Protractor will not attempt to synchronize with the page before performing actions.
	 * This can be harmful because Protractor will not wait until $timeouts and $http calls have been processed,
	 * which can cause tests to become flaky.
	 * This should be used only when necessary, such as when a page continuously polls an API using $timeout.
	 *
	 * @param ignoreSynchronization true if synchronization is to be disabled
	 */
	public void setIgnoreSynchronization(boolean ignoreSynchronization) {
		this.ignoreSynchronization = ignoreSynchronization;
	}

	void waitForAngular() {
		if (!this.isIgnoreSynchronization()) {
			this.jsExecutor.executeAsyncScript(ClientSideScripts.waitForAngular, this.rootElement);
		}
	}
}
