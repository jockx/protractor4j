package net.jockx.protractor4j;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class JavaScriptBy extends By {

	private String script;
	private Object[] args;

	private WebElement rootElement;

	public JavaScriptBy(String script, Object... args) {
		this.script = script;
		this.args = args;
	}

	@Override
	public WebElement findElement(SearchContext context) {
		List<WebElement> elements = this.findElements(context);
		return elements.size() > 0 ? elements.get(0) : null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<WebElement> findElements(SearchContext context) {

		// Create script arguments
		Object[] scriptArgs = new Object[this.args.length + 1];
		scriptArgs[0] = getRootElement();
		System.arraycopy(this.args, 0, scriptArgs, 1, this.args.length);

		// Get JS executor
		JavascriptExecutor jsExecutor = (JavascriptExecutor) context;

		List<WebElement> elements = (List<WebElement>) jsExecutor.executeScript(this.script, scriptArgs);
		if (elements == null) {
			elements = new ArrayList<WebElement>();
		}
		return elements;
	}

	public WebElement getRootElement() {
		return rootElement;
	}

	public void setRootElement(WebElement rootElement) {
		this.rootElement = rootElement;
	}
}
