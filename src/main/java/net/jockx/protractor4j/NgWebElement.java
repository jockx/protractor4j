package net.jockx.protractor4j;

import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsElement;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides a mechanism to get elements off the page for test.
 */
public class NgWebElement implements WebElement, WrapsElement {

	private NgWebDriver ngDriver;
	private WebElement element;

	/**
	 * Creates a new instance of {@link NgWebElement} by wrapping a {@link WebElement} instance.
	 *
	 * @param ngDriver The {@link NgWebDriver} in use.
	 * @param element  The existing {@link WebElement} instance.
	 */
	public NgWebElement(NgWebDriver ngDriver, WebElement element) {
		this.ngDriver = ngDriver;
		this.element = element;
	}

	/**
	 * Clicks this element.
	 */
	@Override
	public void click() {
		this.ngDriver.waitForAngular();
		this.element.click();
	}

	/**
	 * Submits this element to the web server.
	 */
	@Override
	public void submit() {
		this.ngDriver.waitForAngular();
		this.element.submit();
	}

	/**
	 * Simulates typing text into the element.
	 *
	 * @param keysToSend keys to type
	 */
	@Override
	public void sendKeys(CharSequence... keysToSend) {
		this.ngDriver.waitForAngular();
		this.element.sendKeys(keysToSend);
	}

	/**
	 * Clears the content of this element.
	 */
	@Override
	public void clear() {
		this.ngDriver.waitForAngular();
		this.element.clear();
	}

	/**
	 * @return Gets the tag name of this element.
	 */
	@Override
	public String getTagName() {
		this.ngDriver.waitForAngular();
		return this.element.getTagName();
	}

	/**
	 * @param name specified attribute
	 * @return Gets the value of the specified attribute for this element.
	 */
	@Override
	public String getAttribute(String name) {
		this.ngDriver.waitForAngular();
		return this.element.getAttribute(name);
	}

	/**
	 * @return Gets a value indicating whether or not this element is selected.
	 */
	@Override
	public boolean isSelected() {
		this.ngDriver.waitForAngular();
		return this.element.isSelected();
	}

	/**
	 * @return Gets a value indicating whether or not this element is enabled.
	 */
	@Override
	public boolean isEnabled() {
		this.ngDriver.waitForAngular();
		return this.element.isEnabled();
	}

	/**
	 * @return Gets the innerText of this element, without any leading or trailing whitespace,
	 * and with other whitespace collapsed.
	 */
	@Override
	public String getText() {
		this.ngDriver.waitForAngular();
		return this.element.getText();
	}

	/**
	 * Finds all {@link NgWebElement}s using the given mechanism.
	 *
	 * @param by The locating mechanism to use.
	 * @return List of all {@link NgWebElement}s matching the current criteria,
	 *         or an empty list if nothing matches.
	 */
	@Override
	public List<WebElement> findElements(By by) {
		if (by instanceof JavaScriptBy) {
			((JavaScriptBy) by).setRootElement(this.element);
		}
		this.ngDriver.waitForAngular();
		return this.element.findElements(by)
				.stream().map(e -> new NgWebElement(this.ngDriver, e))
				.collect(Collectors.toList());
	}

	/**
	 * Finds the first {@link NgWebElement} using the given mechanism.
	 *
	 * @param by The locating mechanism to use.
	 * @return The first matching {@link NgWebElement} on the current context.
	 * @throws org.openqa.selenium.NoSuchElementException If no element matches the criteria.
	 */
	@Override
	public NgWebElement findElement(By by) {
		if (by instanceof JavaScriptBy) {
			((JavaScriptBy) by).setRootElement(this.element);
		}
		this.ngDriver.waitForAngular();
		return new NgWebElement(this.ngDriver, this.element.findElement(by));
	}

	/**
	 * @return Gets a value indicating whether or not this element is displayed.
	 */
	@Override
	public boolean isDisplayed() {
		this.ngDriver.waitForAngular();
		return this.element.isDisplayed();
	}

	/**
	 * @return Gets a {@link Point} object containing the coordinates of the upper-left corner
	 * of this element relative to the upper-left corner of the page.
	 */
	@Override
	public Point getLocation() {
		this.ngDriver.waitForAngular();
		return this.element.getLocation();
	}

	/**
	 * @return Gets a {@link Dimension} object containing the height and width of this element.
	 */
	@Override
	public Dimension getSize() {
		this.ngDriver.waitForAngular();
		return this.element.getSize();
	}

	/**
	 * @param propertyName CSS property
	 * @return Gets the value of a CSS property of this element.
	 */
	@Override
	public String getCssValue(String propertyName) {
		this.ngDriver.waitForAngular();
		return this.element.getCssValue(propertyName);
	}

	/**
	 * @return Gets the wrapped {@link WebElement} instance.
	 */
	@Override
	public WebElement getWrappedElement() {
		return this.element;
	}

	public Object evaluate(String expression)
	{
		this.ngDriver.waitForAngular();
		return ((JavascriptExecutor)this.ngDriver.getWrappedDriver())
				.executeScript(ClientSideScripts.evaluate, this.element, expression);
	}
}
