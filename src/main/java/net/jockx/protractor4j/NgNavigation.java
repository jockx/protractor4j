package net.jockx.protractor4j;

import java.net.URL;

import static org.openqa.selenium.WebDriver.Navigation;

/**
 * Provides a mechanism for navigating against an AngularJS application.
 */
public class NgNavigation implements Navigation {

	private NgWebDriver ngDriver;
	private Navigation navigation;

	/**
	 * Creates a new instance of {@link NgNavigation} by wrapping a {@link Navigation} instance.
	 *
	 * @param ngDriver   The {@link NgNavigation} in use.
	 * @param navigation The existing {@link Navigation} instance.
	 */
	public NgNavigation(NgWebDriver ngDriver, Navigation navigation) {
		this.ngDriver = ngDriver;
		this.navigation = navigation;
	}


	/**
	 * Move back a single entry in the browser's history.
	 */
	@Override
	public void back() {
		this.navigation.back();
	}

	/**
	 * Move a single "item" forward in the browser's history.
	 */
	@Override
	public void forward() {
		this.navigation.forward();
	}

	/**
	 * Load a new web page in the current browser window.
	 *
	 * @param url The URL to load. It is best to use a fully qualified URL
	 */
	@Override
	public void to(String url) {
		ngDriver.get(url);
	}

	/**
	 * Load a new web page in the current browser window.
	 *
	 * @param url The URL to load.
	 */
	@Override
	public void to(URL url) {
		if (url == null) {
			throw new IllegalArgumentException("URL cannot be null.");
		}
		ngDriver.get(url.toString());
	}

	/**
	 * Refreshes the current page.
	 */
	@Override
	public void refresh() {
		this.navigation.refresh();
	}


	/**
	 * @return The wrapped {@link Navigation} instance.
	 */
	public Navigation getNavigation() {
		return navigation;
	}
}
