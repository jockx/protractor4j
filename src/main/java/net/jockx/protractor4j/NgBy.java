package net.jockx.protractor4j;

import org.openqa.selenium.By;

/**
 * Mechanism used to locate elements within Angular applications by binding, model, etc.
 */
public class NgBy {


	/**
	 * Gets a mechanism to find elements by their Angular binding.
	 *
	 * @param binding The binding, e.g. '{{cat.name}}'.
	 * @return A {@link By} object the driver can use to find the elements.
	 */
	public static By binding(String binding) {
		return new JavaScriptBy(ClientSideScripts.findBindings, binding);
	}

	/**
	 * Gets a mechanism to find input elements by their model name.
	 *
	 * @param model The model name.
	 * @return A {@link By} object the driver can use to find the elements.
	 * @deprecated Use {@link NgBy#model(String)} instead,
	 */
	@Deprecated
	public static By input(String model) {
		return new JavaScriptBy(ClientSideScripts.findModel, model);
	}

	/**
	 * Gets a mechanism to find elements by their model name.
	 *
	 * @param model The model name.
	 * @return A {@link By} object the driver can use to find the elements.
	 */
	public static By model(String model) {
		return new JavaScriptBy(ClientSideScripts.findModel, model);
	}

	/**
	 * Gets a mechanism to find textarea elements by their model name.
	 *
	 * @param model The model name.
	 * @return A {@link By} object the driver can use to find the elements.
	 * @deprecated Use {@link NgBy#model(String)} instead,
	 */
	@Deprecated
	public static By textArea(String model) {
		return new JavaScriptBy(ClientSideScripts.findModel, model);
	}

	/**
	 * Gets a mechanism to find select elements by their model name.
	 *
	 * @param model The model name.
	 * @return A {@link By} object the driver can use to find the elements.
	 * @deprecated Use {@link NgBy#model(String)} instead,
	 */
	@Deprecated
	public static By select(String model) {
		return new JavaScriptBy(ClientSideScripts.findModel, model);
	}

	/**
	 * Gets a mechanism to find select option elements by their model name.
	 *
	 * @param model The model name.
	 * @return A {@link By} object the driver can use to find the elements.
	 */
	public static By selectedOption(String model) {
		return new JavaScriptBy(ClientSideScripts.findSelectedOptions, model);
	}

	/**
	 * Gets a mechanism to find all rows of an ng-repeat.
	 *
	 * @param repeat The text of the repeater, e.g. 'cat in cats'.
	 * @return A {@link By} object the driver can use to find the elements.
	 */
	public static By repeater(String repeat) {
		return new JavaScriptBy(ClientSideScripts.findAllRepeaterRows, repeat);
	}
}
