Protractor4j
============
The Java port of [the .NET port](https://github.com/bbaia/protractor-net) of [Protractor](https://github.com/angular/protractor), an end to end test framework for Angular applications.

It is just a simple copy of the .NET project by bbaia available at [https://github.com/bbaia/protractor-net](https://github.com/bbaia/protractor-net)

Requires java 1.8

Maven
-----
    <dependency>
        <groupId>net.jockx</groupId>
        <artifactId>fluent-page</artifactId>
        <version>1.0</version>
    </dependency>